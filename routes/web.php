<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Admin area
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::get('/', 'Admin\HomeController@index')->name('/');
    Route::get('/profile', 'Admin\ProfileController@index')->name('/profile');
    Route::post('/profile', 'Admin\ProfileController@profilePost')->name('profile');
    Route::post('/addprofilepic', 'Admin\ProfileController@addprofilepic')->name('addprofilepic');
    Route::post('/change-password', 'Admin\ProfileController@changePassword')->name('change-password');
});

// auth urls
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('/logout');
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');

//Lock Screen
Route::get('/lockscreen', 'Auth\LockAccountController@lockscreen')->name('lockscreen');
Route::post('/lockscreen', 'Auth\LockAccountController@unlock')->name('lockscreen');