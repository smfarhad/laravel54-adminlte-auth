<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\User;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware(['auth', 'lock']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $title = 'Profile';
        return view('admin.profile')
                        ->with('title', $title);
    }

    public function profilePost(Request $request) {
        $user_id = Auth::user()->id;
        $input = [];
        $input['name'] = $request->input('name');
        $input['email'] = $request->input('email');
        User::where('id', $user_id)->update($input);
        return Response::json(array('success' => true), 200);
    }

    public function addprofilepic(Request $request) {
        
        $rules = array(
            'profile_pic' => 'required|max:5000|mimes: png,jpg,jpeg,JPG,JPEG,gif,bmp'
            
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Response::json(array(
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 200); // 400 being the HTTP code for an invalid request.
        }

        $upload_file = 'profile_pic';
        $request->file($upload_file)->getClientOriginalExtension();
        if ($request->hasFile($upload_file)) {
            $extension = $request->file($upload_file)->getClientOriginalExtension();
            $new_name = md5(microtime(true));
            $new_name_lower_case_with_ext = strtolower("$new_name.$extension");
            $year = date('Y');
            $month = strtolower(date('F'));
            $day = date('d');
            $file_path = "/uploads/$year/$month/$day/";
            $upload_path = base_path() . $file_path;
            $temp_file = $upload_path . $new_name_lower_case_with_ext;
            $request->file($upload_file)->move($upload_path, $new_name_lower_case_with_ext);
        } else {
            $new_name_lower_case_with_ext = '';
        }
        $img_save = $file_path . $new_name_lower_case_with_ext;
        User::where('id', Auth::user()->id)
                ->update(['profile_pic' => $img_save]);
        return Response::json(array('success' => true, 'redirect_url' => $request->input('redirect_url'), 'path' => $img_save), 200); // 400 being the HTTP code for an invalid request.
    }

    public function changePassword(Request $request) {
        $rules = array('password' => 'required|confirmed',
            'password_confirmation' => 'required');
        $validator = Validator::make($request->all(), $rules);
        // Validate the input and return correct response
        if ($validator->fails()) {
            return Response::json(array(
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray()
                            ), 200); // 400 being the HTTP code for an invalid request.
        }
        User::where('id', Auth::user()->id)
                ->update(['password' => bcrypt($request->input('password'))]);
        return Response::json(array('success' => true), 200);
    }

}
