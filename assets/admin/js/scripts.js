  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  
// save edit profile 
  $(function () {  
    $('form#editProfile').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');
        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType: 'json',
            success: function (response) {
                var password = $("div#password");
                var confirm_password = $("div#confirm_password");
                if (response.success == true) {
                    // remove error essential
                    var message = $('div#message').find('.alert');
                    message.addClass('alert-success');
                    message.html("<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" +
                            '<span><strong>Success!</strong> Profile info updated successfully. </span>');
                    $('div#message').show().delay(300000).fadeOut();
                } else {


                }
            }
        });

    });
  });   
  
  // 
  
  // add profile picture 
    $('form#addPic').on('submit', function (e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: "addprofilepic",
            type: "POST",
            data: formData,
            async: false,
            dataType: "json",
            success: function (response) {
                if(response.success == true){
                    
                    var message = $('div#message').find('.alert');
                    message.addClass('alert-success');
                    message.html("<a href='#' class='close' data-dismiss='alert' aria-label='close' title='close'>X</a>" +
                            '<span><strong>Success!</strong> Image Update Success Fully. </span>');
                    $('div#message').show().delay(3000).fadeOut();
                    var img = $("img.profile_img");
                    img.attr('src', response.path);
                    window.location.href = response.redirect_url;
                   
                } else{
                    var message = $('div#message').find('.alert');
                    message.addClass('alert-danger');
                    message.html("<a href='#' class='close' data-dismiss='alert' aria-label='close' title='close'>X</a>" +
                            '<span><strong>ERROR!</strong> ' + response.errors.profile_pic + '</span>');
                    $('div#message').show().delay(5000).fadeOut();
                }

            },
            cache: false,
            contentType: false,
            processData: false
        });

    });
    
        $('form#changePassword').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        var url = $(this).attr('action');
        // ajax submition 
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            dataType: 'json',
            success: function (response) {
                var password = $("div#password");
                var confirm_password = $("div#confirm_password");
                if (response.success == true) {
                    // remove error essential
                    password.removeClass('has-error');
                    confirm_password.removeClass('has-error');
                    $("span#password_error").text('');
                    $("span#confirm_password_error").text('');


                    var message = $('div#message').find('.alert');
                    message.addClass('alert-success');
                    message.html("<a href='#' class='close' data-dismiss='alert' aria-label='close' title='close'>X</a>" +
                            '<span><strong>Success!</strong> Password updated successfully. </span>');
                    $('div#message').show().delay(5000).fadeOut();
                } else {
                    if (response.errors.password) {
                        password.addClass('has-error');
                        $("span#password_error").text(response.errors.password);
                    } else {
                        password.removeClass('has-error');
                        $("span#password_error").text('');
                    }
                    if (response.errors.password_confirmation) {
                        confirm_password.addClass('has-error');
                        $("span#confirm_password_error").text(response.errors.password_confirmation);
                    } else {
                        confirm_password.removeClass('has-error');
                        $("span#confirm_password_error").text('');
                    }
                }
            }
        });

    });