@extends('auth.master')
@section('content')
<div class="login-box-body">
    <h4 class="login-box-msg">Registration</h4>
    You have successfully registered. An email is sent to you for verification.
</div>
@endsection