@extends('auth.master')
@section('content')
<div class="login-box-body">
    <h4 class="login-box-msg">Registration Confirmed</h4>
   Your Email is successfully verified. Click here to <a href="{{url('/login')}}">login</a>
 
</div>
@endsection