@extends('admin.layout.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Profile
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">User profile</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="{{Auth::user()->profile_pic ? Auth::user()->profile_pic : '/assets/admin/img/avatar5.png'}}" alt="User profile picture">

                        <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>

                        <p class="text-muted text-center">Software Engineer</p>

                        <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#personal-info" data-toggle="tab">Personal Info</a></li>
                        <li><a href="#avatar" data-toggle="tab">Change Avatar</a></li>
                        <li><a href="#cahange-password" data-toggle="tab">Change Password</a></li>

                    </ul>
                    <div class="tab-content">
                        <!--  personal Info-->
                        <div class="active tab-pane" id="personal-info">
                            <form id="editProfile" class="form-horizontal" action="/admin/profile" method="post">
                                {{ csrf_field() }}
                                
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input name="name" value="{{Auth::user()->name}}" type="text" class="form-control" id="name" placeholder="Full Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input name="email"  value="{{Auth::user()->email}}" type="email" class="form-control" id="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <!-- Avatar -->
                        <div class=" tab-pane" id="avatar">
                            <form id="addPic" action="{{ route('addprofilepic') }}" role="form" enctype="multipart/form-data" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="redirect_url" value="{{Request::url()}}">
                                <div class="form-group">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width:160px; height: auto">
                                            <img class="profile_img" style="height:200px; width:150px"
                                                 src="{{Auth::user()->profile_pic ? Auth::user()->profile_pic : '/assets/admin/img/avatar5.png'}}"
                                                 alt="{{ Auth::user()->first_name}} {{ Auth::user()->last_name}}" />
                                        </div>
                                        <div>
                                            <div class="form-group">
                                                <label class="btn btn-primary" for="exampleInputFile">Upload a Profile Picture <i class="fa fa-upload"></i> </label>
                                                <input name="profile_pic" type="file" id="exampleInputFile" style="display:none">
                                                <p class="help-block">  </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix margin-top-10">
                                        <span class="label label-danger">NOTE! </span> &nbsp; &nbsp; &nbsp; <span> Attached
                                            image thumbnail is supported in Latest Firefox,
                                            Chrome, Opera, Safari and Internet Explorer 10 only 
                                        </span>
                                    </div>
                                </div>
                                <div class="margin-top-10">
                                    <input type="submit" class="btn green" value="Submit">
                                </div>
                            </form>

                        </div>

                        <!-- cahange-password -->
                        <div class="tab-pane" id="cahange-password">
                            <form id="changePassword" class="form-horizontal" action="{{ route('change-password') }}" role="form" method="post">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="col-sm-3 control-label">New Password</label>
                                    <div class="col-sm-7">
                                        <input name="password" type="password" class="form-control" id="password" placeholder="Password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-3 control-label">Re-type New Password</label>
                                    <div class="col-sm-7">
                                        <input name="password_confirmation" type="password" class="form-control" id="re-password" placeholder="Retype Password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <button type="submit" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection